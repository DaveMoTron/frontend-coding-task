# Front-end coding task

## Task
ReachOut has loads of traditional infographics which are usually embeded images or PDFs. 

Quite rightly our users tell us these are a bit old school and can suck on mobile, we want to make this infographic easier for young people to access and engage with.

Task requirements: 

1. Take the supplied inforgraphic image and transform it for web. 
https://au.reachout.com/-/media/young-people/images/other/set-goals-like-a-boss-infographic.jpg

2. We want you to make it more engaging to our users than the jpg version.
This could range from hoverstates to custom animation, it is up to you. It just needs to be more engaging than the image. 

Here are some examples of web-infographics that we think are cool. 

1. http://www.goldmansachs.com/our-thinking/pages/millennials/

2. https://blog.adioma.com/what-internet-thinks-based-on-media-infographic/

Notes:
You can use any framework that you want to complete this task. 

## What we are looking for

1. It works. We should be able to open the html file and see this working.
2. Solution design skills. Has taken the breif above and "run with it".  
3. It's responsive. (If you run out of time, Mobile is more imporant than tablet to us)

Note: 
We only want you to spend around 2-3 hours or less on this task.
You can explain any compromises you had to make to get this up and running. i.e What you would do different if you were to deploy this to production?

## Submission
1. Fork the repo https://bitbucket.org/inspirefoundation/frontend-coding-task
2. You can submit your complieted project by sending us the link to your forked repo or submit a pull request.
3. Ensure all source code is commited, not just the final output.



